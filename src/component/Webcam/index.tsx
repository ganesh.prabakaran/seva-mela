import { Component, createRef, RefObject, useEffect, useRef } from "react";
import { render } from "react-dom";

class Webcam extends Component<Props> {
    constructor(props: Props) {
        super(props);
    }
    videoRef: RefObject<HTMLVideoElement> = createRef<HTMLVideoElement>();
    componentDidMount() {
        (async () => {
            const props = {
                audio: false, video: {
                    width: window.innerWidth,
                    facingMode: "user",
                },
            }
            try {
                const camStream = await navigator.mediaDevices.getUserMedia(props);
                if (this.videoRef.current) {
                    this.videoRef.current.srcObject = camStream;
                    this.videoRef.current.onloadedmetadata = () => this.videoRef.current?.play();
                }

            } catch (err: any) {
                console.log(err.name + ": " + err.message);

            }
        })()
    }
    getVideoElem() {
        if (window.innerWidth > 1024) {
            return <video id={this.props.id} ref={this.videoRef} className={'scale-x-[-1] transform-gpu mx-auto invisible'} width={1280} height={720} />
        }
        return <video id={this.props.id} ref={this.videoRef} className={'scale-x-[-1] transform-gpu mx-auto invisible'} width={1024} height={576} />
    }
    render() {

        return <div className="w-[100vw] h-[100vh] flex items-center">
            {
                this.getVideoElem()
            }
        </div>
    }

}

export default Webcam;

type Props = {
    id: string
}