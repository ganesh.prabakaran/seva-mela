import { Keypoint } from "@tensorflow-models/hand-pose-detection";
import { Component } from "react";
import { getBallons, initBallonFall, popBallon } from "../../utils/ballons.ts";
class Ground extends Component<Props> {
    constructor(props: Props) {
        super(props);
        const pin = new Image();
        const pinRight = new Image();
        const ballon = new Image();
        pin.src = '/pin.png';
        pin.width = 90;
        pin.height = 90;
        pin.onload = () => {
            this.pin = pin;
        }
        ballon.src = '/balloon.png';
        ballon.width = 90;
        ballon.height = 130;
        ballon.onload = () => {
            this.ballon = ballon;
        }

        pinRight.src = '/pinRight.png';
        pinRight.width = 90;
        pinRight.height = 90;
        pinRight.onload = () => {
            this.pinRight = pinRight;
        }

    }
    cameraAnimationFrameRefId: number | null = null
    pin: HTMLImageElement | null = null;
    pinRight: HTMLImageElement | null = null;
    ballon: HTMLImageElement | null = null;
    webCam: HTMLVideoElement | null = null;
    pop: HTMLAudioElement = new Audio('/pop.mp3');
    pop1: HTMLAudioElement = new Audio('/pop.mp3');
    drawHands(indexFingers: Hand[]) {
        this.renderHandPoints(indexFingers);
    }
    drawCamera() {
        if (!this.canvasCtx) {
            // this.cameraAnimationFrameRefId = requestAnimationFrame(this.drawCamera);
            return;
        }
        if (!this.webCam) {
            this.webCam = document.getElementById('webcamVideo') as HTMLVideoElement;
        }
        this.canvasCtx.drawImage(this.webCam, 0, 0, this.webCam.videoWidth, this.webCam.videoHeight);
        this.renderBallons();
    }
    getCanvasDimensions() {
        if (window.innerWidth > 1024) {
            return { width: 1280, height: 720 }
        }
        return { width: 1024, height: 576 }
    }

    renderBallons() {
        if (!this.canvasCtx || !this.ballon) {
            return;
        }
        const ballons = getBallons();

        for (let ballon of ballons) {
            this.canvasCtx.drawImage(
                this.ballon, ((ballon.position?.x || 0) - (this.ballon.width / 2)), ((ballon.position?.y || 0) - (this.ballon.height / 2)), this.ballon.width, this.ballon.height);

            // this.canvasCtx.fillStyle = 'Pink';
            // this.canvasCtx.strokeStyle = 'White';
            // this.canvasCtx.lineWidth = 3;
            // this.canvasCtx.beginPath();
            // // this.canvasCtx.arc((ballon.position?.x || 0) - 2, (ballon.position?.y || 0) - 2, 3, 0, 2 * Math.PI);
            // this.canvasCtx.rect((ballon.position?.x || 0) - 12.5, (ballon.position?.y || 0) - 12.5, 25, 25);
            // this.canvasCtx.fill();
        }
    }
    renderHandPoints(indexFingers: Hand[]) {
        if (!this.canvasCtx) {
            return;
        }
        else {
            for (let fingerTip of indexFingers) {
                if (fingerTip.handedness === 'Left') {
                    if (!this.pin) {
                        console.log('pin not loaded')
                        return;
                    }

                    this.canvasCtx.drawImage(
                        this.pin, ((fingerTip.indexFingerTip?.x || 0) - (this.pin.width / 2)), ((fingerTip.indexFingerTip?.y || 0) - (this.pin.height / 2)), this.pin.width, this.pin.height);

                    if (this.ballon) {
                        const fingerRect: rectangle = [((fingerTip.indexFingerTip?.x || 0) - (this.pin.width / 2)), ((fingerTip.indexFingerTip?.y || 0) - (this.pin.height / 2)), this.pin.width, this.pin.height];
                        const ballons = getBallons();
                        for (let ballon of ballons) {
                            const ballonRect: rectangle = [((ballon.position?.x || 0) - (this.ballon.width / 2)), ((ballon.position?.y || 0) - (this.ballon.height / 2)), this.ballon.width, this.ballon.height]
                            if (areRectanglesOverlap(fingerRect, ballonRect)) {
                                popBallon(ballon);
                                if (this.pop.paused)
                                    this.pop.play();
                                else
                                    this.pop1.play();

                            }

                        }
                    }
                }
                else {
                    if (!this.pinRight) {
                        console.log('pin not loaded')
                        return;
                    }
                    this.canvasCtx.drawImage(
                        this.pinRight, ((fingerTip.indexFingerTip?.x || 0) - (this.pinRight.width / 2)), ((fingerTip.indexFingerTip?.y || 0) - (this.pinRight.height / 2)), this.pinRight.width, this.pinRight.height);
                    if (this.ballon) {
                        const fingerRect: rectangle = [((fingerTip.indexFingerTip?.x || 0) - (this.pinRight.width / 2)), ((fingerTip.indexFingerTip?.y || 0) - (this.pinRight.height / 2)), this.pinRight.width, this.pinRight.height];
                        const ballons = getBallons();
                        for (let ballon of ballons) {
                            const ballonRect: rectangle = [((ballon.position?.x || 0) - (this.ballon.width / 2)), ((ballon.position?.y || 0) - (this.ballon.height / 2)), this.ballon.width, this.ballon.height]
                            if (areRectanglesOverlap(fingerRect, ballonRect)) {
                                popBallon(ballon);
                                if (this.pop.paused)
                                    this.pop.play();
                                else
                                    this.pop1.play();
                            }
                        }
                    }
                }
            }
        }
    }
    canvasCtx: CanvasRenderingContext2D | null = null;
    componentDidMount() {
        (async () => {
            const canvas: HTMLCanvasElement = document.getElementById(this.props.id) as HTMLCanvasElement;
            if (canvas) {
                this.canvasCtx = canvas.getContext('2d');
            }
        })();
        initBallonFall({
            top: 0,
            left: 0,
            right: this.getCanvasDimensions().width,
            bottom: this.getCanvasDimensions().height
        });
    }

    render() {

        return <div className="top-0 left-0 fixed w-[100vw] h-[100vh] flex items-center">
            <canvas id={this.props.id} className={'scale-x-[-1] transform-gpu mx-auto'} width={this.getCanvasDimensions().width} height={this.getCanvasDimensions().height} />
        </div>
    }
    // render() {
    //     return <canvas id={this.props.id} className={'fixed top-0 left-0 scale-x-[-1] transform-gpu invisible'} width={1024} height={720}>
    //     </canvas >
    // }

}

export default Ground;

type Props = {
    id: string
}

type Hand = {
    handedness: string,
    indexFingerTip: Keypoint | undefined
}
const areRectanglesOverlap = (rect1: rectangle, rect2: rectangle) => {
    let [left1, top1, right1, bottom1] = [rect1[0], rect1[1], rect1[2] + rect1[0], rect1[3] + rect1[1]],
        [left2, top2, right2, bottom2] = [rect2[0], rect2[1], rect2[2] + rect2[0], rect2[3] + rect2[1]];
    // The first rectangle is under the second or vice versa
    if (
        (top1 < top2 && bottom1 < top2)
        ||
        (top2 < top1 && bottom2 < top1)

    ) {
        return false;
    }
    // The first rectangle is to the left of the second or vice versa
    if (
        (left1 < left2 && right1 < left2)
        ||
        (left2 < left1 && right2 < left1)

    ) {
        return false;
    }
    // Rectangles overlap
    return true;
}

type rectangle = [number, number, number, number];
