import { useEffect, useRef, useState } from 'react'
import './App.css'
import Webcam from './component/Webcam/index.tsx'
import { Hand, HandDetector, createDetector, SupportedModels } from '@tensorflow-models/hand-pose-detection';
import '@tensorflow/tfjs-backend-webgl';
import * as tf from '@tensorflow/tfjs-core';
import {VERSION} from '@mediapipe/hands';
import * as tfjsWasm from '@tensorflow/tfjs-backend-wasm';
import Ground from './component/ground/index.tsx';

tfjsWasm.setWasmPaths(
  `https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-backend-wasm@${tfjsWasm.version_wasm}/dist/`);

function App() {
  const [count, setCount] = useState(0)
  const groundRef = useRef<Ground>(null);
  const [isReady, setIsReady] = useState<true | null | undefined>(undefined);
  useEffect(() => {
    let rafId: number | null = null;
    const initializeBackend = async () => {
      const backendName = 'gpu';
      const ENGINE = tf.engine();
      if (!(backendName in ENGINE.registryFactory)) {
        console.log(ENGINE.registryFactory);
        throw new Error(`${backendName} backend is not registed.`);
      }
      if (backendName in ENGINE.registry) {
        const backendFactory = tf.findBackendFactory(backendName);
        tf.removeBackend(backendName);
        tf.registerBackend(backendName, backendFactory);
      }
      await tf.setBackend(backendName);
    };
    const estimateResult = async (detector: HandDetector) => {
      if (rafId) {
        try { window.cancelAnimationFrame(rafId); } catch { }
      }
      const video: HTMLVideoElement = document.querySelector("#webcamVideo") as HTMLVideoElement;
      if (!video)
        return;
      if (video && video.readyState < 2) {
        await new Promise((resolve) => {
          video.onloadeddata = () => {
            resolve(video);
          };
        });
      }
      let hands: Hand[] = [];
      if (detector != null) {
        try {
          hands = await detector.estimateHands(
            video,
            { flipHorizontal: false });
        } catch (error) {
          console.log('error in estimating', error);
        }
      }
      if (groundRef.current) {
        console.log('canvas draw init')
        groundRef.current.drawCamera();
      }
      if (hands.length > 0) {
        if (groundRef.current) {
          const indexTips = hands.map(x => ({
            ...x,
            indexFingerTip: x.keypoints.find((x: any) => x.name === 'index_finger_tip')
          }))
          groundRef.current.drawHands(indexTips);
        }
      }
    }
    let detector: HandDetector | null = null;

    const tid = setTimeout(async () => {
      // await initializeBackend();
      console.log('creatting dector')
      try {
        detector = await createDetector(SupportedModels.MediaPipeHands, {
          runtime: 'mediapipe',
          modelType: 'full',
          maxHands: 2,
          solutionPath: `https://cdn.jsdelivr.net/npm/@mediapipe/hands@${VERSION}`
        });
        setIsReady(true);
        const getResult = async () => {
          if (detector)
            await estimateResult(detector);
          rafId = requestAnimationFrame(getResult);
        };
        getResult();
      } catch (err) {
        setIsReady(null);
        console.log('err in creating detector', err)
      }
    }, 3000);
    return () => {
      clearTimeout(tid);
      detector?.dispose();
      if (rafId !== null)
        window.cancelAnimationFrame(rafId)
    }
  }, [])
  return (
    <div className="App">
      <Webcam id={'webcamVideo'} />
      <Ground ref={groundRef} id={'playground'} />
      {
        isReady === undefined &&
        <div className='top-0 left-0 fixed w-[100vw] h-[100vh] z-10 flex items-center justify-center'>
          <h1 className='font-bold text-9xl uppercase' >
            loading...
          </h1>
        </div>
      }
      {
        isReady === null &&
        <div className='top-0 left-0 fixed w-[100vw] h-[100vh] z-10 flex items-center justify-center'>
          <h1 className='font-bold text-9xl uppercase' >
            :) Failed
          </h1>
        </div>
      }
    </div>
  )
}

export default App

type rectangle = [number, number, number, number];
const areRectanglesOverlap = (rect1: rectangle, rect2: rectangle) => {
  let [left1, top1, right1, bottom1] = [rect1[0], rect1[1], rect1[2], rect1[3]],
    [left2, top2, right2, bottom2] = [rect2[0], rect2[1], rect2[2], rect2[3]];
  // The first rectangle is under the second or vice versa
  if (
    (top1 < top2 && bottom1 < top2)
    ||
    (top2 < top1 && bottom2 < top1)
  ) {
    return false;
  }
  // The first rectangle is to the left of the second or vice versa
  if (
    (left1 < left2 && right1 < left2)
    ||
    (left2 < left1 && right2 < left1)

  ) {
    return false;
  }
  // Rectangles overlap
  return true;
}
