class Ballon {
    constructor(boundry: Boundry) {
        this.boundry = boundry;
        this.position = this.initPosition();
    }
    boundry: Boundry;
    position: Keypoint;
    initPosition() {
        if (!this.position) {
            return {
                x: (Math.random() * (this.boundry.right - this.boundry.left) + this.boundry.left),
                y: (Math.random() * (
                    // (this.boundry.top * 10 / 100)
                    this.boundry.bottom
                    - this.boundry.top) + this.boundry.top),
            }
        } else return this.position;
    }
    isOffScreen() {
        return (
            this.position.x > this.boundry.right
            ||
            this.position.x < this.boundry.left
            ||
            this.position.y > this.boundry.bottom
            ||
            this.position.y < this.boundry.top
        )
    }
}
const MAX_BALLOONS = 10;
let baloons: Ballon[] = [];
export const getBallons = () => baloons;
function generateBallon(boundry: Boundry) {
    if (baloons.length < MAX_BALLOONS)
        baloons.push(new Ballon(boundry));
}
const moveBaloons = () => {
    for (let baloon of baloons) {
        baloon.position.y = baloon.position?.y + (Math.random() * ((10) - 2) + 2);
        baloon.position.x = baloon.position?.x + (Math.random() * ((10) - (-10)) + (-10));
        if (baloon.isOffScreen())
            popBallon(baloon);
    }
}
export function popBallon(baloon: Ballon) {
    baloons = baloons.filter(x => x !== baloon)
}
export const initBallonFall = (boundry: Boundry) => {
    setInterval(() => {
        generateBallon(boundry)
    }, 1000)
    setInterval(() => {
        moveBaloons();
    }, 200)
}
type Boundry = {
    top: number,
    left: number,
    bottom: number,
    right: number
};
type Keypoint = {
    x: number,
    y: number
};

